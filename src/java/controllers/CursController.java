/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Curs;
import model.CursDAO;

/**
 * CursController
 *
 * @author Alejandro Asensio
 * @version 2019-02-07
 */
@WebServlet(name = "CursController", urlPatterns = {"/cursos"})
public class CursController extends HttpServlet {

    private String path;
    private CursDAO pdao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // calcula el ruta absoluta para llegar a WEB-INF 
        // Cuando hacemos Clean & Build, se genera otra estructura de directorios: LoginApplication/build/web/WEB-INF/
        path = getServletContext().getRealPath("/WEB-INF");
        pdao = new CursDAO(path);

        if (request.getParameter("action") != null) {
            String action = request.getParameter("action");
            switch (action) {
                case "load":
                    load(request, response);
                    break;
                case "form_filterCursosCategoria":
                    form_filterCursosCategoria(request, response);
                    break;
                case "filterCursosOnline":
                    filterCursosOnline(request, response);
                    break;
                case "filterCursosPresencials":
                    filterCursosPresencial(request, response);
                    break;
                default:
                    throw new AssertionError();
            }
        } else {
            response.sendRedirect("index.jsp");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Loads the curss data.
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void load(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Call the DAO, which calls the DataBase to get the data from the file
        List<Curs> cursos = pdao.listAll();
        request.setAttribute("cursos", cursos);
        request.setAttribute("users", null);
        RequestDispatcher rd = request.getRequestDispatcher("landing.jsp");
        rd.forward(request, response);
    }

    /**
     * Shows the form to filter among cursos.
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void form_filterCursosCategoria(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("form_filterCursosCategoria", "yes");
        RequestDispatcher rd = request.getRequestDispatcher("landing.jsp");
        rd.forward(request, response);
    }

    /**
     * Filters the cursos by some search criteria.
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void filter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Get form parameters
        String classification = request.getParameter("category");

        // Prepare the object to perform the query
//        Curs p = new Curs();
//        p.setClassification(classification);
        // p.setMenopause(menopause.equals("si"));
        // p.setMenopause(false);
//        p.setMenopauseType(menopauseType);
        
//        List<Curs> filteredCurss = pdao.listFiltered(p);
//        request.setAttribute("cursos", filteredCurss);
        RequestDispatcher rd = request.getRequestDispatcher("landing.jsp");
        rd.forward(request, response);
        
    }
    
    
    
    private void filterCursosOnline(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Prepare the object to perform the query
//        Curs p = new Curs();
//        p.setClassification(classification);
        // p.setMenopause(menopause.equals("si"));
        // p.setMenopause(false);
//        p.setMenopauseType(menopauseType);
        
        List<Curs> filteredCurs = pdao.listFilteredType("ONLINE");
        request.setAttribute("cursos", filteredCurs);
        System.out.println("filterCursosOnline");
        RequestDispatcher rd = request.getRequestDispatcher("landing.jsp");
        rd.forward(request, response);
    }
    
        
    private void filterCursosPresencial(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Prepare the object to perform the query
//        Curs p = new Curs();
//        p.setClassification(classification);
        // p.setMenopause(menopause.equals("si"));
        // p.setMenopause(false);
//        p.setMenopauseType(menopauseType);
        
        List<Curs> filteredCurs = pdao.listFilteredType("PRESENCIAL");
        request.setAttribute("cursos", filteredCurs);
        System.out.println("filterCursosPresencials");
        RequestDispatcher rd = request.getRequestDispatcher("landing.jsp");
        rd.forward(request, response);
        
    }
    
    /* 
     case "filterCursosOnline":
                    filterCursosOnline(request, response);
                    break;
                case "filterCursosPresencial":
                    filterCursosPresencial(request, response);
                    break;
    */
    
}
