package model;

/**
 * Models a Curs register.
 * @author INS Provençana
 * @version 1.0, 18-02-2020
 */
public class Curs {

    private String Id;
    // private String TipusCurs;
    private String TipusCurs;
    // private String CategoriaCurs;
    private String CategoriaCurs;
    private String Títol;
    private String Descripcio;
    private String DataInici;

    public Curs(String Id, String TipusCurs, String CategoriaCurs, String Títol, String Descripcio, String DataInici) {
        this.Id = Id;
        this.TipusCurs = TipusCurs;
        this.CategoriaCurs = CategoriaCurs;
        this.Títol = Títol;
        this.Descripcio = Descripcio;
        this.DataInici = DataInici;
    }

    @Override
    public String toString() {
        return "Curs{" + "Id=" + Id + ", TipusCurs=" + TipusCurs + ", CategoriaCurs=" + CategoriaCurs + ", T\u00edtol=" + Títol + ", Descripcio=" + Descripcio + ", DataInici=" + DataInici + '}';
    }
   
    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getTipusCurs() {
        return TipusCurs;
    }

    public void setTipusCurs(String TipusCurs) {
        this.TipusCurs = TipusCurs;
    }

    public String getCategoriaCurs() {
        return CategoriaCurs;
    }

    public void setCategoriaCurs(String CategoriaCurs) {
        this.CategoriaCurs = CategoriaCurs;
    }

    public String getTítol() {
        return Títol;
    }

    public void setTítol(String Títol) {
        this.Títol = Títol;
    }

    public String getDescripcio() {
        return Descripcio;
    }

    public void setDescripcio(String Descripcio) {
        this.Descripcio = Descripcio;
    }

    public String getDataInici() {
        return DataInici;
    }

    public void setDataInici(String DataInici) {
        this.DataInici = DataInici;
    }

            
   

}
