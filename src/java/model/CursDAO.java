package model;

import java.util.ArrayList;
import java.util.List;
import model.Curs;
import model.Curs;

/**
 * Curs DAO to cursos.csv
 * @author INS Provençana
 * @version 1.0, 18-02-2020
 */
public class CursDAO {

    private DataBase d; // object of class DataBase

    public CursDAO() {
    }

    public CursDAO(String path) {
        d = new DataBase(path + "/files/cursos.csv");
    }

    /**
     * Check if a given course is registered in the courses file.
     *
     * @param curs Curs to be found
     * @return boolean true if curs's registerId is in file; false otherwise
     */
    public boolean find(Curs curs) {
        boolean found = false;
        List<String> all = d.listAllLines();
        for (String s : all) {
            String[] pieces = s.split(";");
            if (pieces[0].equals(curs.getId())) {
                found = true;
                break;
            }
        }
        return found;
    }

    /**
     * Lists all CURSOS in database.
     *
     * @return a List of Curs objects
     */
    public List listAll() {
        List<Curs> cursos = new ArrayList<>();
        List<String> all = d.listAllLines();

        // Id,TipusPub,Categoria,Títol,Descripció,DataInici
        for (String s : all) {
            String[] pieces = s.split(";");
//            Curs c = new Curs();

            String id = pieces[0];
            String TipusCurs = pieces[1];
            String Categoria = pieces[2];
            String Titol = pieces[3];
            String Descripcio = pieces[4];
            String DataInici = pieces[5];
            // double imc = Double.parseDouble(pieces[5]);
            //int menarche = Integer.parseInt(pieces[7]);
            //boolean menopause = pieces[8].toUpperCase().equals("SI");

            cursos.add(new Curs(id, TipusCurs, Categoria, Titol, Descripcio, DataInici));
        }

        return cursos;
    }

    public List listFilteredCategory(String category) {
        List<Curs> allCursos = listAll();
        List<Curs> filteredCursos = new ArrayList<>();
        
        for (Curs c : allCursos) {
            boolean match = 
               c.getCategoriaCurs().equals(category);
            if (match) {
                filteredCursos.add(c);
            }
        }
        return filteredCursos;
    }
    
    /**
     * Filtrar els Cursos per tipus: Online o Presencial
     * @param type => ONLINE O PRESENCIAL
     * @return 
     */
    public List listFilteredType(String type) {
        List<Curs> allCurs = listAll();
        List<Curs> filteredCurs = new ArrayList<>();
        
        if(type.equals("ONLINE")||
                type.equals("PRESENCIAL")) {
            // 
            for (Curs curs : allCurs) {
                boolean match = 
                   curs.getTipusCurs().equals(type);
                if (match) {
                    filteredCurs.add(curs);
                }
            }
        }
        return filteredCurs;
    }
    

    public static void main(String[] args) {
        // El test només funciona a Linux
        CursDAO testDao = 
          new CursDAO(
              "/home/tarda/NetBeansProjects/SimpleIntranetWebApp/web/WEB-INF/");
        for(Object cursObj : testDao.listAll()) {
            System.out.println(" testDao = " + cursObj);
        }
    }

}
