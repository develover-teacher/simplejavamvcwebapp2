<%@page import="java.util.List"%>
<%@page import="model.Curs"%>
<%
    if (request.getAttribute("cursos") != null) {
        List<Curs> cursos = (List<Curs>) request.getAttribute("cursos");
        StringBuilder dynamicTable = new StringBuilder("");
        if (session.getAttribute("username") == null) {
//            dynamicTable.append("<table><thead><tr>"
//                    + "<th scope='col'>Age Group</th>"
//                    + "<th scope='col'>Weight</th>"
//                    + "<th scope='col'>IMC</th>"
//                    + "<th scope='col'>Classification</th>"
//                    + "</tr></thead><tbody>");
//            for (Patient patient : patients) {
//                dynamicTable.append(String.format("<tr>"
//                        + "<td scope='row'>%s</td>"
//                        + "<td>%d</td>"
//                        + "<td>%.2f</td>"
//                        + "<td>%s</td>"
//                        + "</tr>",
//                        patient.getAgeGroup(),
//                        patient.getWeight(),
//                        patient.getImc(),
//                        patient.getClassification()));
//            }
        } else {
            dynamicTable.append("<table><thead><tr>"
                    + "<th scope='col'>Id</th>"
                    + "<th scope='col'>TipusPub</th>"
                    + "<th scope='col'>Categoria</th>"
                    + "<th scope='col'>T�tol</th>"
                    + "<th scope='col'>Descripci�</th>"
                    + "<th scope='col'>DataInici</th>"
                    + "</tr></thead><tbody>");
            for (Curs curs : cursos) {
                dynamicTable.append(String.format("<tr>"
                        + "<td scope='row'>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "</tr>",
                        curs.getId(),
                        curs.getTipusCurs(),
                        curs.getCategoriaCurs(),
                        curs.getT�tol(),
                        curs.getDescripcio(),
                        curs.getDataInici()));
            }
        }
        out.println(dynamicTable.toString());
    }
%>